CREATE DATABASE IF NOT EXISTS record_of_student_success;
USE record_of_student_success;
CREATE TABLE IF NOT EXISTS student
(id  INT PRIMARY KEY UNIQUE NOT NULL,
surname VARCHAR(20) NOT NULL,
name VARCHAR(20) NOT NULL,
date_of_birth DATE NOT NULL,
address VARCHAR(100) NOT NULL,
autobiography TEXT NOT NULL,
rating DOUBLE,
stipend DOUBLE,
student_group_id INT(11) NOT NULL,
specialty_id INT(11) NOT NULL,
module_id INT(11) NOT NULL,
score_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS student_group
(id INT PRIMARY KEY UNIQUE NOT NULL,
group_name VARCHAR(45) NOT NULL,
year_of_enter INT(4) NOT NULL,
specialty_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS specialty
(id INT PRIMARY KEY UNIQUE NOT NULL,
name VARCHAR(45) NOT NULL,
subject_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS subject
(id INT PRIMARY KEY UNIQUE NOT NULL,
name VARCHAR(45) NOT NULL,
type_of_control ENUM('exam', 'credit'))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS lecturer
(id INT PRIMARY KEY UNIQUE NOT NULL,
surname VARCHAR(45) NOT NULL,
name VARCHAR(45) NOT NULL,
subject_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS module
(id INT PRIMARY KEY UNIQUE NOT NULL,
name  VARCHAR(45) NOT NULL,
result_number1 INT,
result_number2 INT,
subject_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

CREATE TABLE IF NOT EXISTS score
(id INT PRIMARY KEY UNIQUE NOT NULL,
semester_evaluation_50_score INT,
semester_evaluation_100_score INT,
specialty_id INT(11) NOT NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;

ALTER TABLE record_of_student_success.student
ADD FOREIGN KEY(student_group_id) REFERENCES student_group(id),
ADD FOREIGN KEY(specialty_id) REFERENCES specialty(id),
ADD FOREIGN KEY(module_id) REFERENCES module(id),
ADD FOREIGN KEY(score_id) REFERENCES score(id);

ALTER TABLE record_of_student_success.score
ADD FOREIGN KEY(specialty_id) REFERENCES subject(id);

ALTER TABLE record_of_student_success.student_group
ADD FOREIGN KEY(specialty_id) REFERENCES specialty(id);

ALTER TABLE record_of_student_success.specialty
ADD FOREIGN KEY(subject_id) REFERENCES subject(id);

ALTER TABLE record_of_student_success.lecturer
ADD FOREIGN KEY(subject_id) REFERENCES subject(id);

ALTER TABLE record_of_student_success.module
ADD FOREIGN KEY(subject_id) REFERENCES subject(id);